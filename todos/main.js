class Observable {
  constructor() {
    this.observers = [];
  }

  subscribe(f) {
    this.observers.push(f);
  }

  unsubscribe(f) {
    this.observers = this.observers.filter((subscriber) => subscriber !== f);
  }

  notify(data) {
    this.observers.forEach((observer) => observer(data));
  }
}

class TodosModel extends Observable {
  constructor() {
    super();
    this._todos = [
      {
        id: 1,
        text: "Купить продукты",
        done: false,
      },
    ];
  }

  get todos() {
    return this._todos;
  }

  get _newId() {
    return this._todos.length ? this._todos[this._todos.length - 1].id + 1 : 1;
  }

  addTodo(todoData) {
    const newTodo = {
      id: this._newId,
      text: todoData.text || "DEF",
      done: todoData.done || false,
    };
    this._todos.push(newTodo);
    this.notify(this._todos);
  }

  toggleTodo(id) {
    let todoToToggle = this._todos.find((t) => t.id === id);
    todoToToggle.done = !todoToToggle.done;
    this.notify(this._todos);
  }

  deleteTodo(id) {
    this._todos = this._todos.filter((todo) => todo.id !== id);
    this.notify(this._todos);
  }
}

const todoTemplates = {
  todos: function (todos) {
    let tpl = "";
    todos.forEach((todo) => {
      tpl += this.todo(todo);
    });
    return tpl;
  },
  todo: function (todo) {
    return `
      <li
        class="${todo.done ? "todo--done" : ""} todo list-group-item"
      >
        <div class="todo__body">
          <input
            type="checkbox"
            class="todo__check"
            todo-toggler
            value="${todo.id}"
            ${todo.done ? "checked" : ""}
          >
          <span class="todo__text">${todo.text}</span>
        </div>
				<i class="fas fa-trash-alt todo__remove" todo-remove="${todo.id}"></i>
			</li>
    `;
  },
};

class TodosView {
  constructor(element, model) {
    this.element = element;
    this.model = model;
    this.todosList = this.element.querySelector(".todos__list");
    this.todosInput = this.element.querySelector(".todos-form__input");
    this.todosAddBtn = this.element.querySelector(".todos-form__btn");
    this.todoTogglerSelector = "[todo-toggler]";
    this.todoRemoveBtnAttributeName = "todo-remove";
    this.todoRemoveBtnSelector = `[${this.todoRemoveBtnAttributeName}]`;
    this.init();
  }

  init() {
    this.render(this.model.todos);
    this.model.subscribe((todos) => {
      this.render(todos);
    });
    this.listen();
  }

  render(todos) {
    this.todosList.innerHTML = todoTemplates.todos(todos);
  }

  addTodo() {
    const newTodoText = this.todosInput.value;
    if (!newTodoText) {
      return;
    }
    this.model.addTodo({ text: newTodoText });
  }

  toggleTodo(todoEl) {
    if (todoEl.matches(this.todoTogglerSelector)) {
      const id = +todoEl.value;
      if (!id || isNaN(id)) {
        return;
      }
      this.model.toggleTodo(id);
    }
  }

  deleteTodo(todoEl) {
    if (todoEl.matches(this.todoRemoveBtnSelector)) {
      const id = +todoEl.getAttribute(this.todoRemoveBtnAttributeName);
      if (!id || isNaN(id)) {
        return;
      }
      this.model.deleteTodo(id);
    }
  }

  listen() {
    this.todosAddBtn.addEventListener("click", () => {
      this.addTodo();
    });

    this.todosList.addEventListener("change", (e) => {
      const todoEl = e.target;
      this.toggleTodo(todoEl);
    });

    this.todosList.addEventListener("click", (e) => {
      const todoEl = e.target;
      this.deleteTodo(todoEl);
    });
  }
}

// class ReverterModel extends Observable {
//   constructor() {
//     super();
//   }
// }

// class ReverterView {
//   constructor(element, model) {
//     this.element = element;
//     this.model = model;
//     this.revertBtnSelector = "[revert]";
//     this.template = (text) => {
//       return `${text} <button class="reverter__btn" revert>Отменить</button>`;
//     };
//     this.model.subscribe((text, cb) => {
//       this.render(text);
//       this.listen(cb);
//     });
//   }

//   render(text) {
//     this.element.innerHTML = text;
//   }

//   listen() {
//     this.element.addEventListener("click", (e) => {
//       const target = e.target;
//       if (target.matches(this.revertBtnSelector)) {
//         cb();
//         this.clean();
//       }
//     });
//   }

//   clean() {
//     this.element.innerHTML = "";
//   }
// }

function initComponent(selector, View, Model) {
  const elements = document.querySelectorAll(`[data-component=${selector}]`);
  elements.forEach((element) => {
    const model = new Model();
    new View(element, model);
  });
}
initComponent("todos", TodosView, TodosModel);
