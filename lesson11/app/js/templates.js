export function templates() {
  const templates = {};
  templates["todos-container"] = function () {
    return `
      <div class="todos__form">
        Todos Form
      </div>
      <div class="todos__list">
        TodosList
      </div>
    `;
  };
  return templates;
}
