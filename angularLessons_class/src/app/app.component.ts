import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  // title = 'Some title';
  // items = ['Element 1', 'Element 2', 'Element 3'];
  // x = true;
  // counter = 0;
  // increment() {
  //   this.counter++;
  // }
  // decrement() {
  //   this.counter--;
  // }

  name: string;
  age: number;
  isSomething: boolean;
  stringItems: string[];

  user1: IUser;
  users: IUser[];

  user2: User;

  constructor() {
    this.name = 'Ivan';
    this.age = 30;
    this.isSomething = true;
    this.stringItems = ['String 1', 'String 2'];

    this.user1 = {
      name: 'Bob',
      age: 30,
    };

    this.user2 = {
      name: 'Ivane',
      age: 40,
    };

    this.users = [
      {
        name: 'Bob',
        age: 30,
      },
      {
        name: 'Kate',
        email: 'kate@gmail.com',
        age: 20,
      },
    ];
  }

  makeSmth(x: number, y: string) {}
}

class User {
  name: string;
  age: number;
  // constructor() {}

  // method() {}
}

interface IUser {
  name: string;
  email?: string;
  age: number;
}
