// $.ajax({
//   type: "GET",
//   url: "https://api.github.com/users",
//   success: function (users) {
//     console.log(users);
//   },
// });

(async function () {
  const API_URL = "https://api.github.com";
  // const testLogin = "mojombo";

  function getUser(login) {
    return $.get(`${API_URL}/users/${login}`);
  }

  function getUsers() {
    return $.get(`${API_URL}/users`);
  }

  const users = await getUsers();
  console.log(users);
  const selectedUser = await getUser(users[2].login);
  console.log(selectedUser);

  // getUsers().then((users) => {
  //   console.log("!!!!");
  //   console.log(users);
  // });

  // getUsers().then((users) => {
  //   const userLoginToGet = users[5].login;
  //   console.log("users", users);
  //   getUser(userLoginToGet).then((user) => {
  //     console.log("user", user);
  //   });
  // });
})();

// const httpPromise = new Promise((resolve, reject) => {
//   // http call
//   setTimeout(() => {
//     resolve("data");
//   }, 300);
// });

// httpPromise.then((res) => {
//   console.log(res);
// });
