export class TodosView {
    constructor(el, providers) {
        this.$host = $(el);
        this.$list = this.$host.find(".todos__list");
        this.$todoText = this.$host.find(".todos-form__input");
        this.$addTodo = this.$host.find(".todos-form__add");
        this.todosModel = providers.todosModel;
        this.todosModel.subscribe((data) => {
            this.render(data);
        });
        this.listen();

    }

    listen() {
        this.listenAdding();
        this.listenDeleteTodo();
    }

    listenAdding() {
        this.$addTodo.click(() => {
            const todoText = this.$todoText.val();
            if (!todoText) {
                return;
            }
            const newTodo = {
                text: todoText,
            };
            this.todosModel.add(newTodo);
        });
    }
    listenDeleteTodo() {
            this.$list.click((event) => {
                if (event.target.className === 'item__btn') {
                    const id = parseInt(event.target.dataset.id);
                    this.todosModel.remove(id);
                }
            })
        }
        // listenSearch() {
        //   const foundTodos = this.todosModel.getFilteredBySearch(searchQuery);
        //   this.render(foundTodos);
        // }

    render(todos) {
        let tpl = "";
        todos.forEach((todo) => {
            tpl += `
        <div class="todos__item todo">${todo.text}  
        <button class="item__btn" data-id=${todo.id}>
        Delete
        </button>
        </div> 
      `;
        });
        this.$list.html(tpl);
    }
}