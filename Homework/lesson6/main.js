// const TodosModel = (function() {
//     let todos = [{
//         id: 1,
//         text: "Make smth",
//         done: false,
//     }, ];
//     return {
//         getTodos: function(cb) {

//             cb(todos);
//         },
//         addTodo: function(newTodoText, cb) {
//             const newTodo = {
//                 id: !todos.length ? 1 : todos[todos.length - 1].id + 1,
//                 text: newTodoText,
//                 done: false,
//             };
//             todos.push(newTodo);
//             cb(newTodo);
//         },
//         deleteTodo: function(id, cb) {
//             todos = todos.filter(todo => todo.id !== id)
//             cb(todos);

//         }
//     };
// })();

// const TodosView = (function() {
//     const componentRef = document.querySelector("#todos");
//     const addBtnElement = componentRef.querySelector(".todos__btn");
//     const inputElement = componentRef.querySelector(".todos__input");
//     const listElement = componentRef.querySelector(".todos-list");


//     const getTodoTemplate = function(todo) {
//         return `<div class="todos-list__item">${todo.text} <button class="list__button" data-id=${todo.id}> Delete </button></div>`;
//     };

//     return {
//         listenAddingTodo: function(cb) {
//             addBtnElement.addEventListener("click", () => {
//                 if (!inputElement.value) {
//                     return;
//                 }
//                 const todoText = inputElement.value;
//                 cb(todoText);
//             });
//         },
//         listenDeleteTodo: function(cb) {
//             listElement.addEventListener('click', (event) => {
//                 if (event.target.className === 'list__button') {
//                     const id = parseInt(event.target.dataset.id)
//                     cb(id);
//                 }
//             });


//         },
//         renderTodos: function(todos) {
//             let template = "";
//             todos.forEach((todo) => {
//                 template += getTodoTemplate(todo);
//             });
//             listElement.innerHTML = template;


//         },
//         renderNewTodo: function(todo) {
//             listElement.insertAdjacentHTML("beforeend", getTodoTemplate(todo));
//         },
//     };
// })();

// const TodosController = (function() {
//     return {
//         init: function() {
//             TodosModel.getTodos((todos) => {
//                 TodosView.renderTodos(todos);
//             });
//             this.listenAdd();
//             this.listenDelete();

//         },
//         listenAdd: function() {
//             TodosView.listenAddingTodo((todoText) => {
//                 TodosModel.addTodo(todoText, (todo) => {
//                     TodosView.renderNewTodo(todo);
//                 });
//             });
//         },
//         listenDelete: function() {
//             TodosView.listenDeleteTodo((id) => {
//                 TodosModel.deleteTodo(id, (todos) => {
//                     TodosView.renderTodos(todos);
//                 });
//             });
//         },
//     }
// })();

// TodosController.init();


class TodosModel {
    constructor() {
        this.todos = [{
            id: 1,
            text: "Make smth",
            done: false,
        }, ];
    }
    getTodos(cb) {
        cb(this.todos);
    }
    addTodo(newTodoText, cb) {
        const newTodo = {
            id: !this.todos.length ? 1 : this.todos[this.todos.length - 1].id + 1,
            text: newTodoText,
            done: false,
        };
        this.todos.push(newTodo);
        cb(newTodo);
    }
    deleteTodo(id, cb) {
        this.todos = this.todos.filter(todo => todo.id !== id)
        cb(this.todos);

    }

};

class TodosView {
    constructor() {
        this.componentRef = document.querySelector("#todos");
        this.addBtnElement = this.componentRef.querySelector(".todos__btn");
        this.inputElement = this.componentRef.querySelector(".todos__input");
        this.listElement = this.componentRef.querySelector(".todos-list");
        this.getTodoTemplate = function(todo) {
            return `<div class="todos-list__item">${todo.text} <button class="list__button" data-id=${todo.id}> Delete </button></div>`;
        };
    }

    listenAddingTodo(cb) {
        this.addBtnElement.addEventListener("click", () => {
            if (!this.inputElement.value) {
                return;
            }
            const todoText = this.inputElement.value;
            cb(todoText);
        });
    }
    listenDeleteTodo(cb) {
        this.listElement.addEventListener('click', (event) => {
            if (event.target.className === 'list__button') {
                const id = parseInt(event.target.dataset.id)
                cb(id);
            }
        });


    }
    renderTodos(todos) {
        let template = "";
        todos.forEach((todo) => {
            template += this.getTodoTemplate(todo);
        });
        this.listElement.innerHTML = template;


    }
    renderNewTodo(todo) {
        this.listElement.insertAdjacentHTML("beforeend", this.getTodoTemplate(todo));
    }

}

class TodosController {
    constructor(model, view) {
        this.model = model
        this.view = view
        this.init()
    }
    init() {
        this.model.getTodos((todos) => {
            this.view.renderTodos(todos);
        });
        this.listenAdd();
        this.listenDelete();

    }
    listenAdd() {
        this.view.listenAddingTodo((todoText) => {
            this.model.addTodo(todoText, (todo) => {
                this.view.renderNewTodo(todo);
            });
        });
    }
    listenDelete() {
        this.view.listenDeleteTodo((id) => {
            this.model.deleteTodo(id, (todos) => {
                this.view.renderTodos(todos);
            });
        });
    }
}

const app = new TodosController(new TodosModel, new TodosView)