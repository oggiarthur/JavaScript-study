// const TodosComponent = (function () {
//   const elementRef = document.querySelector("#todos");
//   const listRef = elementRef.querySelector(".todos-list");

//   const todos = [
//     {
//       text: "Купить продукты",
//       done: false,
//     },
//     {
//       text: "Проверить ДЗ",
//       done: true,
//     },
//   ];

//   function renderTodos() {
//     let todosTpl = "";
//     todos.forEach((todo) => {
//       todosTpl += `<div class="todos-list__item ${
//         todo.done ? "todos-list__item--isDone" : ""
//       }">${todo.text}</div>`;
//     });
//     listRef.innerHTML = todosTpl;
//   }

//   function addNewTodo(todo) {
//     listRef.insertAdjacentHTML(
//       "beforeend",
//       `<div class="todos-list__item ${
//         todo.done ? "todos-list__item--isDone" : ""
//       }">${todo.text}</div>`
//     );
//   }

//   return {
//     addTodo: function (todo) {
//       todos.push(todo);
//       addNewTodo(todo);
//     },
//     init: function () {
//       renderTodos();
//     },
//     getTodos: function () {
//       return todos;
//     },
//   };
// })();

// TodosComponent.init();

// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// console.log(TodosComponent.getTodos());

// MVC (MVVM...)
// ___________
// Model
// View
// Controller

// View -> Controller -> Model -> Controller -> View

const Model = (function() {
    const data = [];
    return {
        addData: function(newData, cb) {
            const todo = {
                id: this.data.length > 0 ? this.todos[this.data.length - 1].id + 1 : 1,
                text: todoText,
                complete: false,
            }
            this.data.push(todo);
            cb(data);
        },
        editTodo(id, updatedText) {
            this.data = this.data.map(todo =>
                todo.id === id ? { id: todo.id, text: updatedText, complete: todo.complete } : todo
            )
        },
        removeData: function(removeIndex, cb) {

            data.splice(removeIndex, 1);
            cb(data);
        },
        toggleTodo(id) {
            this.todos = this.data.map(todo =>
                todo.id === id ? { id: todo.id, text: todo.text, complete: !todo.complete } : todo
            )
        },
        getData: function() {
            return data;
        },
    };
})();

const View = (function() {
    return {
        listen: function() {
            // addEventListener ...
            const dataFromViewToBeAdded = "New data";
            Controller.addData(dataFromViewToBeAdded);
        },
        render: function(data) {
            // innerHTML ...
        },
        renderNewData: function(newData) {},
    };
})();

const Controller = (function() {
            return {
                init: function() {
                    const dataToRender = Model.getData();
                    View.render(dataToRender);
                    View.listen();
                },
                addData: function(dataFromView) {
                    Model.addData(dataFromView, (newData) => {
                        View.renderNewData(newData);
                    });
                },
                removeData: function(dataFromView) {
                    // мы слушаем если нажата кнопочка Done
                    Model.removeData(dataFromView, (newData) => {
                        View.renderNewData(newData);
                    });
                }
            })();

        Controller.init();

        // let a = 10;
        // let b = 20;
        // let obj = { a: a, b: b };
        // console.log(obj);

        // function foreach(arr, cb) {
        //   for (let i = 0; i < arr.length; i++) {
        //     const element = arr[i];
        //     cb(element);
        //   }
        // }

        // foreach(["sdfsd", "ddd"], (el) => {
        //   console.log(el);
        // });

        // ["sdfsd", "ddd"].forEach((el) => {
        //   console.log(el);
        // });

        // Сделать клон http://todomvc.com/examples/angular2/ по MVC
        // 1) Добавление
        // 2) Когда отмечаем как сделанное - удалять из списка

        // ПЕРВЫМ ЭТАПОМ НАПИШИТЕ МОДЕЛЬ
        // ПОТОМ КОНТРОЛЛЕР
        // ВО VIEW ВСЕ ВЫВОДИТЕ В КОНСОЛЬ
        // И ТОЛЬКО ПОТОМ РЕНДЕРИТЕ html