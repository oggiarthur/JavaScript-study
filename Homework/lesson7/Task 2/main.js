$(document).ready(function() {

        $('div.tabs button').click(function() {
            var tab_id = $(this).attr('data-tab');

            $('.tabs-control').removeClass('active');
            $('.tab').removeClass('active');

            $(this).addClass('active');
            $('[data-tab=' + tab_id + ']').addClass('active');
        })

    })
    // 1) Пример counter (mvc) все cb (колбек функции) переписать на промисы (new Promise)
    // 2) Реализовать табы. Есть несколько блоков текста и есть соответствующие кнопки под каждый текст
    // При нажатии на кнопку показывается соответствующий этой кнопке текст, предидущий кусок текста скрывается