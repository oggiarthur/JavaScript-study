export function templates() {
    const templates = {};

    templates["todos-container"] = function() {
        return `
      <div class="todos__form">
        Todos Form
      </div>
      <div class="todos__list">
        TodosList
      </div>
    `;
    };



    templates["todos__list"] = function(todos) {

        return todos.map((todo) => (
            `<div class = "todos__item todo" > ${todo.text }
          <button class = "item__btn"data - id = ${todo.id } >Delete
          </button>
          </div>
        `
        ));
    };
    return templates;
}