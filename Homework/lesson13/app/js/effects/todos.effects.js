import { GOT_TODOS, TO_GET_TODOS, ADD_TO_TODOS, ADD_TODO } from "../actions/constants"
import { useEffect } from "./index"

import { TodosService } from "../services/todos.service";

class TodosEffects {
    constructor(properties) {
        this.service = properties.service;
    }

    getTodos() {
        useEffect(TO_GET_TODOS);

        this.service.getTodos().then((res) => {
            useEffect(GOT_TODOS, res)
        });
    }

    addTodos(todo) {
        useEffect(ADD_TO_TODOS);

        this.service.addTodo(todo).then((res) => {
            console.log('res: ', res);

            useEffect(ADD_TODO, res)
        })
    }
}

const todosEffects = new TodosEffects({ service: new TodosService() })
export default todosEffects;