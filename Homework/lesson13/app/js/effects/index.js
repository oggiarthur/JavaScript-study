import TodosEffects from "./todos.effects";

function useEffect(actionType, payload) {
    window.store.dispatch({ type: actionType, payload });
}

export { TodosEffects, useEffect };