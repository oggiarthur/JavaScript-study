export const TO_GET_TODOS = "TRY_TO_GET_TODOS";
export const GOT_TODOS = "GOT_TODOS";
export const ADD_TO_TODOS = "TRY_TO_ADD_TODOS"
export const ADD_TODO = "ADD_TODO";
export const REMOVE_TODO = "REMOVE_TODO";
export const servicesAPI = "https://jsonplaceholder.typicode.com/todos";