class Observable {
    constructor() {
        this.observers = [];
    }
    subscribe(f) {
        this.observers.push(f);
    }
    unsubscribe(obsToUnsubscribe) {
        this.observers = this.observers.filter(obs => obs !== obsToUnsubscribe);
    }
    notify(data) {
        this.observers.forEach(o => {
            o(data);
        })
    }
}
class TodosModel extends Observable {
    constructor() {
        super();
        this.todos = [{
            id: 1,
            text: "Make smth",
            done: false,
        }, ];
    }
    getTodos() {
        return this.todos;
    }
    addTodo(newTodoText) {
        const newTodo = {
            id: !this.todos.length ? 1 : this.todos[this.todos.length - 1].id + 1,
            text: newTodoText,
            done: false,
        };
        this.todos.push(newTodo);
        this.notify(this.todos);
    }
    deleteTodo(id) {
        this.todos = this.todos.filter(todo => todo.id !== id)
        this.notify(this.todos);
    }
};

class TodosView {
    constructor(model) {
        this.model = model;
        this.listenAddingTodo();
        this.listenDeleteTodo();
        this.model.subscribe((data) => {
            this.renderTodosList(data);
        })
        this.init();
    }
    init() {
        this.renderTodosList(this.model.getTodos());
    }

    listenAddingTodo() {
        $('#todos .todos__btn').click(() => {
            console.log('listenAddingTodo');
            this.model.addTodo($('#todos .todos__input').val());
            console.log(this.model.getTodos());
        })
    }
    listenDeleteTodo() {
        $('.todos-list').click((event) => {
            if (event.target.className === 'list__button') {
                const id = parseInt(event.target.dataset.id)
                this.model.deleteTodo(id);
            }
        })
    }

    renderTodoItem(text, id) {
        return (
            `<div class="todos-list__item">
                ${text}
                <button class="list__button" data-id=${id}>
                    Delete
                </button>
            </div>`
        );
    }

    renderTodosList(todos) {
        let newData = '';
        todos.forEach((todo) => {
            newData += this.renderTodoItem(todo.text, todo.id);
        });
        $('.todos-list').html(newData);
    }

}
const todosModel = new TodosModel();
const list = new TodosView(todosModel);


// 1) Сделать todos приложение с функционалом: добавление, удаление

// Observable
// TodosModel extends Observable (не забудьте вызвать super())
// TodosView (render - будет перерендеривать весь список)