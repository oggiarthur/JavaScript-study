import { todosTemplate } from "./todos.template";

export class TodosComponent {
    constructor(properties) {
        this.host = $('<div class="todos"></div>');
        this.store = properties.store;
        this.todos = this.store.getState();
        this.render();
        this.handleEvents();
        this.store.subscribe(() => {
            this.render();
        });
    }

    render() {
        this.host.html(todosTemplate(this.store.getState()));
    }

    handleEvents() {
        this.host.on("click", ".todo__remove", (e) => {
            const id = +$(e.target).attr("data-id");
            this.store.dispatch({ type: "REMOVE_TODO", id });
        });

        this.host.on("click", ".check", (e) => {
            const id = +$(e.target).attr("data-id");
            this.store.dispatch({ type: "DONE", id });
        });
    }

    html() {
        return this.host;
    }
}