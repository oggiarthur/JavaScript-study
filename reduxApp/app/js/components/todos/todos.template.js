export function todosTemplate(todos) {
    let tpl = "";
    todos.forEach((t) => {
        tpl += `<div class="todo ${t.done && 'done'}"> <input type="checkbox" ${t.done && 'checked'} class="check" data-id="${t.id}">${t.id} - ${t.text} <button class="todo__remove" data-id="${t.id}">Remove</button></div>`;
    });
    return tpl;
}