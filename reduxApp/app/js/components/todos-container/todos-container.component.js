import { TodosComponent } from "../todos/todos.component";
import { todosContainerTemplate } from "./todos-container.template";
import { TodosFormComponent } from "../todos-form/todos-form.component";

export class TodosContainer {
  constructor(properties) {
    this.host = $('<div class="todos"></div>');
    this.store = properties.store;
    this.render();
  }

  render() {
    this.host.html(todosContainerTemplate());
    this.host
      .find(".todos__list")
      .html(new TodosComponent({ store: this.store }).html());
    this.host
      .find(".todos__form")
      .html(new TodosFormComponent({ store: this.store }).html());
  }

  html() {
    return this.host;
  }
}
