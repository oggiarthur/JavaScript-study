export function todosContainerTemplate() {
  return `
    <div class="todos__form"></div>
    <div class="todos__list"></div>
  `;
}
