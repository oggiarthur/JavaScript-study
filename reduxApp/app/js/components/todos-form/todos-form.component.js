import { todosFormTemplate } from "./todos-form.template";

export class TodosFormComponent {
  constructor(properties) {
    this.host = $('<div class="todos-form"></div>');
    this.store = properties.store;
    this.render();
  }

  render() {
    this.host.html(todosFormTemplate());
    this.handleEvents();
  }

  handleEvents() {
    this.host.find(".todos-form__add").click(() => {
      const inputText = this.host.find(".todos-form__input").val();
      if (inputText.length) {
        this.store.dispatch({ type: "ADD_TODO", text: inputText });
      }
    });
  }

  html() {
    return this.host;
  }
}
