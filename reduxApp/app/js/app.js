import { todosReducer } from "./reducers/todos.reducer";
import { createStore } from "redux";
import { TodosContainer } from "./components/todos-container/todos-container.component";

const store = createStore(todosReducer);
$("#app").html(new TodosContainer({ store }).html());