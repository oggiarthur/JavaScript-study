const initialState = [{
        id: 1,
        text: "Learn Redux",
        done: false,
    },

];

export function todosReducer(state = initialState, action) {
    switch (action.type) {
        case "ADD_TODO":
            return [
                ...state,
                {
                    id: state.length ? state[state.length - 1].id + 1 : 1,
                    text: action.text,
                    done: false,
                },
            ];
        case "REMOVE_TODO":
            return state.filter((t) => t.id !== action.id);
        case "DONE":
            state.forEach((item) => {
                if (action.id === item.id) {
                    item.done = !item.done;
                }
            })
            return state;
        default:
            return state;
    }
}