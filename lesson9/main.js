class Observable {
  constructor() {
    this.observers = [];
  }

  subscribe(f) {
    this.observers.push(f);
  }

  unsubscribe(obsToUnsubscribe) {
    this.observers = this.observers.filter(obs => obs !== obsToUnsubscribe);
  }

  notify(data) {
    this.observers.forEach(o => {
      o(data);
    })
  }
}

class CounterModel extends Observable {
  constructor() {
    super();
    this.number = 0;
  }

  increment() {
    this.number++;
    this.notify(this.number);
  }
}

class CounterView {
  constructor(model) {
    this.model = model;

    this.model.subscribe((data) => {
      this.render(data);
    })

    this.listen();
  }

  listen() {
    $('#counter .counter__btn--plus').click(() => {
      this.model.increment();
    })
  }

  render(data) {
    $('#counter .counter__number').html(data);
    // $('.el').append(`<div></div>`)
  }
}

const counterModel = new CounterModel();
const counter = new CounterView(counterModel);



//     O
//   /   \
// C       C



// 1) Сделать todos приложение с функционалом: добавление, удаление

// Observable
// TodosModel extends Observable (не забудьте вызвать super())
// TodosView (render - будет перерендеривать весь список)