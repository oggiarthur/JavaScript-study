(function() {
  jQuery.fn.scrollToId = function(o) {
    const links = $(this);
    const options = {
      scrollSpeed: 100,
      ...o
    };
    links.click((e) => {
      e.preventDefault();

      const el = $(e.target);
      const elementToScroll = $(el.attr('href'));
      const offsetTop = elementToScroll.offset().top;
      $('html').animate({
        scrollTop: offsetTop
      }, options.scrollSpeed);
    })
  }
}())
