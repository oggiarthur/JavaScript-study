// const area = document.querySelector(".area");

// area.addEventListener("click", (e) => {
//   const target = e.target;
//   console.log(e);
//   const figure = document.createElement("div");
//   figure.classList.add("figure");
//   figure.style.top = e.offsetY + "px";
//   figure.style.left = e.offsetX + "px";
//   target.appendChild(figure);
// });

// renderFigure(figure) {

// }

// const TodosComponent = (function () {
//   const elementRef = document.querySelector("#todos");
//   const listRef = elementRef.querySelector(".todos-list");

//   const todos = [
//     {
//       text: "Купить продукты",
//       done: false,
//     },
//     {
//       text: "Проверить ДЗ",
//       done: true,
//     },
//   ];

//   function renderTodos() {
//     let todosTpl = "";
//     todos.forEach((todo) => {
//       todosTpl += `<div class="todos-list__item ${
//         todo.done ? "todos-list__item--isDone" : ""
//       }">${todo.text}</div>`;
//     });
//     listRef.innerHTML = todosTpl;
//   }

//   function addNewTodo(todo) {
//     listRef.insertAdjacentHTML(
//       "beforeend",
//       `<div class="todos-list__item ${
//         todo.done ? "todos-list__item--isDone" : ""
//       }">${todo.text}</div>`
//     );
//   }

//   return {
//     addTodo: function (todo) {
//       todos.push(todo);
//       addNewTodo(todo);
//     },
//     init: function () {
//       renderTodos();
//     },
//     getTodos: function () {
//       return todos;
//     },
//   };
// })();

// TodosComponent.init();

// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// TodosComponent.addTodo({ text: "Позвонитть Ване", done: false });
// console.log(TodosComponent.getTodos());

// MVC (MVVM...)
// ___________
// Model
// View
// Controller

// View -> Controller -> Model -> Controller -> View

const Model = (function() {
    const data = [];
    return {
        addData: function(newData, cb) {
            data.push(newData);
            //Когда что-то сделалось (сервер добавил данные)
            const dataToRenderInView = data[data.length - 1];
            cb(dataToRenderInView);
        },
        removeData: function(removeIndex) {
            data.splice(removeIndex, 1);
        },
        getData: function() {
            return data;
        },
    };
})();

const View = (function() {
    return {
        listen: function() {
            // addEventListener ...
            const dataFromViewToBeAdded = "New data";
            Controller.addData(dataFromViewToBeAdded);
        },
        render: function(data) {
            // innerHTML ...
        },
        renderNewData: function(newData) {},
    };
})();

const Controller = (function() {
    return {
        init: function() {
            const dataToRender = Model.getData();
            View.render(dataToRender);
            View.listen();
        },
        addData: function(dataFromView) {
            Model.addData(dataFromView, (newData) => {
                View.renderNewData(newData);
            });
            // Model.addData(dataFromView);
            // const dataToRender = Model.getData();
            // View.render(dataToRender);
        },
    };
})();

Controller.init();

// let a = 10;
// let b = 20;
// let obj = { a: a, b: b };
// console.log(obj);

// function foreach(arr, cb) {
//   for (let i = 0; i < arr.length; i++) {
//     const element = arr[i];
//     cb(element);
//   }
// }

// foreach(["sdfsd", "ddd"], (el) => {
//   console.log(el);
// });

// ["sdfsd", "ddd"].forEach((el) => {
//   console.log(el);
// });

// Сделать клон http://todomvc.com/examples/angular2/ по MVC
// 1) Добавление
// 2) Когда отмечаем как сделанное - удалять из списка

// ПЕРВЫМ ЭТАПОМ НАПИШИТЕ МОДЕЛЬ
// ПОТОМ КОНТРОЛЛЕР
// ВО VIEW ВСЕ ВЫВОДИТЕ В КОНСОЛЬ
// И ТОЛЬКО ПОТОМ РЕНДЕРИТЕ html