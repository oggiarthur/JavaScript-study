import { TodosEffects } from "./todos.effects";

function useEffect(actionType /*...*/) {
  // ...
}

export { TodosEffects, useEffect };
